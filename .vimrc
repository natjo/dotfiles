syntax on
let mapleader = "\<Space>"
set background=dark
filetype plugin indent on

"""""""""""""""""""""""""""""""""""""""""
"""""""""""""""PLUGINS"""""""""""""""""""
"""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.vim/plugged')
" File Browser
Plug 'scrooloose/nerdtree'

" Commenter
Plug 'tpope/vim-commentary'

" autocomplete
Plug 'maralla/completor.vim'
" Plug 'ervandew/supertab'

" Restore view after opening a file
Plug 'vim-scripts/restore_view.vim'

" Swap windows easily
Plug 'wesQ3/vim-windowswap'

" Open/Close pairs automatically
Plug 'jiangmiao/auto-pairs'

" t/T/f/F over multiple lines
Plug 'dahu/vim-fanfingtastic'

" Improve status line
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Change background of hex colors to what they define
Plug 'chrisbra/Colorizer'

" Color scheme
"Plug 'sjl/badwolf'
Plug 'ayu-theme/ayu-vim'

" yaml folding
Plug  'pedrohdz/vim-yaml-folds'

" features for terraform files
Plug 'hashivim/vim-terraform'

" features for terraform files
Plug 'whiteinge/diffconflicts'

call plug#end()


"""""""""""""""""""""""""""""""""""""""""
"""""""""""""COLOR SCHEME SETTINGS"""""""
"""""""""""""""""""""""""""""""""""""""""

set termguicolors     " enable true colors support
let ayucolor="mirage" " dark, light, mirage
colorscheme ayu

"""""""""""""""""""""""""""""""""""""""""
"""""""""""""PLUGIN SETTINGS"""""""""""""
"""""""""""""""""""""""""""""""""""""""""

"""""" NERDTREE
nnoremap <leader>d :NERDTreeToggle<CR>
autocmd BufEnter * lcd %:p:h
"close vim if the only window left is a NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" close nerdtree when opening a new window
let g:NERDTreeQuitOnOpen=1

"""""" commentary
nmap <leader>c gc
vmap <leader>c gc
" copy line to above or below and comment the original one out
nmap <leader>cj Ygccp
nmap <leader>ck Ygcckp

"""""" completor
function! Tab_Or_Complete() abort
  " If completor is already open the `tab` cycles through suggested completions.
  if pumvisible()
    return "\<C-N>"
  " If completor is not open and we are in the middle of typing a word then
  " `tab` opens completor menu.
  elseif col('.')>1 && strpart( getline('.'), col('.')-2, 3 ) =~ '^[[:keyword:][:ident:]]'
    return "\<C-R>=completor#do('complete')\<CR>"
  else
    " If we aren't typing a word and we press `tab` simply do the normal `tab`
    " action.
    return "\<Tab>"
  endif
endfunction
inoremap <expr> <Tab> Tab_Or_Complete()
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<cr>"

"""""" vim-windowswap
let g:windowswap_map_keys = 0 "prevent default bindings
nnoremap <silent> <leader>yw :call WindowSwap#MarkWindowSwap()<CR>
nnoremap <silent> <leader>pw :call WindowSwap#DoWindowSwap()<CR>
nnoremap <silent> <leader>w :call WindowSwap#EasyWindowSwap()<CR>j

""""" vim airline
let g:airline_powerline_fonts = 1
let g:airline_section_y = ""  "shows the formatting, I don't care about that
" let g:airline_theme='dark' " or light
" let g:powerline_pycmd = "py3"

""""" Colorizer
let g:colorizer_x11_names = 1
:let g:colorizer_auto_filetype='css,html'
noremap <leader>CC      :ColorToggle<Cr>

"""""""""""""""""""""""""""""""""""""""""
"""""""""""""GENERAL SETTINGS""""""""""""
"""""""""""""""""""""""""""""""""""""""""
" Handle unkown filetypes
autocmd BufRead,BufNewFile *.tpp :setlocal filetype=cpp
autocmd BufRead,BufNewFile *.njk set filetype=html
autocmd BufRead,BufNewFile *.dockerfile set syntax=dockerfile

" Delete all empty lines in the end
autocmd BufWritePre * :%s/\s\+$//e

" Autosave if focus lost, only supported in terminal version
au BufLeave,FocusLost * :wa

" restore_view needs this
set viewoptions=cursor,folds,slash,unix

set timeoutlen=300

" For smooth scrolling
set so=5

" Show last command
set showcmd

" Show wildmenu TODO: do I want more settings for this?
set wildmenu

" Activate line numbers
set ruler
set nu
set relativenumber

" brace face
set showmatch
set matchtime=3

" More responsive split
set splitbelow
set splitright

" Resize splits when the window is resized
au VimResized * :wincmd =

" change directory when entering file
set autochdir

" Use spaces instead of tabs
set expandtab

" Better intendation
set autoindent
set smarttab
set tabstop=4
set shiftwidth=4

" Searching
set incsearch
set hlsearch
set ignorecase
set smartcase

" Replacing
set gdefault

" vi sucks
set nocompatible

" Improve undo tasks
set history=1000
set undofile
set undodir=~/.vim/undo
set undolevels=2000
set undoreload=10000

" Save swap and backup files neatly at one place
set backupdir=~/.vim/backup
set directory=~/.vim/swapfiles//

" Highlight the 80th column
"highlight ColorColumn guibg=gray15
"let &colorcolumn="80"

let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

" Fix an error with the powerline font
let g:powerline_pycmd="py3"

""""""""""""""KEY MAPPINGS"""""""""""""""
"""""""""""""""""""""""""""""""""""""""""
inoremap jk <esc>

" Built-in autocomplete (only required if no autocomplete plugin is configured)
" function! InsertTabWrapper()
"     let col = col('.') - 1
"     if !col || getline('.')[col - 1] !~ '\k'
"         return "\<tab>"
"     else
"         return "\<c-n>"
"     endif
" endfunction
" inoremap <expr> <tab> InsertTabWrapper()
" inoremap <s-tab> <c-p>
" " make enter accept autocomplete
" inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Save current file with enter key
nnoremap <cr> :w<cr>

" Give arrow keys a better purpose
noremap <down> ddp
noremap <up> ddkP
vnoremap <down> dp
vnoremap <down> dkP

" TODO: Folding
setlocal foldmethod=syntax
nnoremap < za
nnoremap > zA
nnoremap <leader>< zM
nnoremap <leader>> zR
autocmd FileType gitcommit setlocal nofoldenable

" more convenient way to use markers
" noremap 'a `a
" noremap `a 'a
" noremap 'b `b
" noremap ``b 'b
" noremap '. `a
" noremap `` ''

" Stop highlighting
nnoremap <esc> :noh<return><esc>
nnoremap <esc>^[ <esc>^[

" Toggle line and column highlighting
nnoremap <leader>hc :set cursorcolumn!<cr>
nnoremap <leader>hl :set cursorline!<cr>
" This will highlight the current line and keep it highlighted even when the cursor moves
" Bonus: 'l will jump to the highlighted line
nnoremap <silent> <leader>hL ml:execute 'match Search /\%'.line('.').'l/'<CR>
nnoremap <leader>hM :match<CR>

" Format json objects
nnoremap <leader>Fj :%!python -m json.tool<CR>

" For split selection
nnoremap <c-h> <c-w>h
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-l> <c-w>l

" go easily through the loclist
nnoremap <leader>K :lprevious<cr>
nnoremap <leader>J :lnext<cr>

" Double space opens last file again
noremap <leader><leader> :w<CR>:e#<CR>

" Close window
nnoremap <leader>q :q<cr>
" Close terminal mode
:tnoremap <Esc> <C-\><C-n>

" Copy&paste
nnoremap <leader>p "+p
vnoremap <leader>y "+y

" Replace a word with the earlier yanked text
" Won't delete the yanked word from the buffer
xnoremap <expr> p 'pgv"'.v:register.'y`>'
xnoremap <expr> P 'Pgv"'.v:register.'y`>'

" Quickly change to global replace mode
nnoremap <leader>r :%s/

" Quick lines
nnoremap <leader>o o<esc>^Dk
nnoremap <leader>O O<esc>^Dj

" Jump easily between last edits
nnoremap <tab> g;
nnoremap <s-tab> g,

" mapping to make movements operate on 1 screen line in wrap mode
function! ScreenMovement(movement)
    if &wrap
        return "g" . a:movement
    else
        return a:movement
    endif
endfunction

" Scrolling always fucks up my code
set mouse=a

" Use # as comment string in cmake files
autocmd FileType cmake setl cms=#%s

" Count number of occurences of word under cursor
map <leader>* *<C-O>:%s///n<CR>

" search through highlighted text
vnoremap // y/\V<C-R>=escape(@",'/\')<CR><CR>

" select current line without newline
noremap vv 0v$h

onoremap <silent> <expr> j ScreenMovement("j")
onoremap <silent> <expr> k ScreenMovement("k")
onoremap <silent> <expr> 0 ScreenMovement("0")
onoremap <silent> <expr> ^ ScreenMovement("^")
onoremap <silent> <expr> $ ScreenMovement("$")
nnoremap <silent> <expr> j ScreenMovement("j")
nnoremap <silent> <expr> k ScreenMovement("k")
nnoremap <silent> <expr> 0 ScreenMovement("0")
nnoremap <silent> <expr> ^ ScreenMovement("^")
nnoremap <silent> <expr> $ ScreenMovement("$")

" Log all key presses to analyze user behaviour
" See https://github.com/EgZvor/vim-sensei
augroup VimSensei
autocmd VimEnter * call ch_logfile($HOME . "/.vim/log/log-" . strftime('%s'))
autocmd InsertEnter * call ch_log("::Entering Insert Mode::")
autocmd InsertLeave * call ch_log("::Leaving Insert Mode::")
autocmd CmdwinEnter * call ch_log("::Entering command-line window::")
autocmd CmdwinLeave * call ch_log("::Leaving command-line window::")
autocmd CmdlineEnter * call ch_log("::Entering command-line mode::")
autocmd CmdlineLeave * call ch_log("::Leaving command-line mode::")
augroup END
