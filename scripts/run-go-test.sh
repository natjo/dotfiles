#! /bin/bash

# Runs only tests matching the first parameter provided.
# I mainly use this wrapper script because I added autocomplete to it to propose test names in $HOME/.oh-my-zsh/completions/_run-go-test
# Completion options can be retrieved by running this with `_completion` argument.
# Requires `grc` available: https://github.com/garabik/grc

set -e

main(){
    # Check argument
    case "$1" in
        "_completion")
        print_completion
        ;;
        *)
        run_test $1
        ;;
    esac
}

run_test(){
    local testName=$1
    echo "=> Run only tests with names containing \"$testName\""
    grc go test ./... -run $testName
}

print_completion(){
    # Look for all functions starting with "Test" in all go files
    grep -ZroPh --include \*.go '(?<=func )Test[^(]+' | tr '\n' ' '
}


main $@
