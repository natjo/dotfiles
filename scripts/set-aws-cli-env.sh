#!/bin/bash

# Call lije this:
#   source set-aws-cli-env "arn:aws:iam::<account-id>:<device-mfa>"
# The first parameter has to be the identification number of the MFA device

main() {
    # Config
    SERIAL_NUMBER="$1"

    if [[ -z "$SERIAL_NUMBER" ]]; then
        echo "Error: identification number of the MFA device not provided as first parameter"
        # We can't call exit here, since this script is meant to be sourced and thus would exit the main shell
        return
    fi

    # Script
    TMP_OUTPUT_JSON="$(mktemp)"

    echo -n "MFA Code: "
    read mfaCode

    aws sts get-session-token --serial-number $SERIAL_NUMBER --token-code $mfaCode --output json > $TMP_OUTPUT_JSON

    export AWS_ACCESS_KEY_ID=$(jq -r .Credentials.AccessKeyId $TMP_OUTPUT_JSON)
    export AWS_SECRET_ACCESS_KEY=$(jq -r .Credentials.SecretAccessKey $TMP_OUTPUT_JSON)
    export AWS_SESSION_TOKEN=$(jq -r .Credentials.SessionToken $TMP_OUTPUT_JSON)

    # Clean up
    rm $TMP_OUTPUT_JSON
}

main $@
