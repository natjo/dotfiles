#!/bin/python3

import os
import sys
from datetime import datetime, timedelta, timezone
from pprint import pprint
from urllib.parse import urljoin

import requests
from dateutil.parser import parse

BASE_URL = "https://api.clockify.me"
# You can get these ids by requesting the endpoint /user
# Or by introspecting some calls on the Clockify UI
USER_ID = "5c01044fb0798723e0eff538"
WORKSPACE_ID = "5c01044fb0798723e0eff539"
WORK_PROJECT_ID = "60b4a0bfa2129925766d4b9d"

# Overtime from before I tracked through clickify
ADDITIONAL_OVERTIME = 9.5

EXPECTED_WORKHOURS = 7.75
# EXPECTED_WORKHOURS = 8
OLDEST_DATE = "2023-1-1"


def print_usage():
    print("clockify-ctrl <action>")
    print()
    print("Automates interaction with clockify.me")
    print(f"<action> must be one of")
    print("- toggle")
    print("- info")
    print("- daily-average")


def main():
    # Collect config
    action = parse_args()
    api_key = os.environ.get("CLOCKIFY_API_KEY")
    api_key = "NzNkNDMzMDctZjE4Yy00NDk4LTkxNjgtY2FiNWM1MmUwNjQ4"
    if api_key == None:
        # Try to load from .env file
        api_key = os.popen("grep CLOCKIFY_API_KEY $HOME/.env | cut -d'=' -f2").read().strip().strip("'")
        if not api_key:
            print('Error! API key at env variable "CLOCKIFY_API_KEY" must be set.')
            exit(1)

    # Information about the latest time entry is always relevant
    latest_time_entry = get_latest_time_entry(api_key)
    if action == "toggle":
        if latest_time_entry == None:
            print("Start timer")
            start_timer(api_key)
        else:
            print("Stop timer")
            stop_timer(
                latest_time_entry["id"],
                latest_time_entry["start"],
                get_current_time_iso_zero_offset(),
                api_key,
            )
    elif action == "info":
        if latest_time_entry == None:
            print("No timer running")
        else:
            start_time = parse(latest_time_entry["start"])
            current_time = datetime.now(timezone.utc)
            print(pretty_time_delta(current_time - start_time))
    elif action == "daily-average":
        print_average_stats(api_key)
    else:
        print_usage()


# If a timer is already running, this will stop it and start a new one
def start_timer(api_key):
    endpoint = f"workspaces/{WORKSPACE_ID}/time-entries"
    data = {"projectId": WORK_PROJECT_ID}
    response = post_clockify_request(endpoint, api_key, data)


def stop_timer(timer_id, start_time, end_time, api_key):
    endpoint = f"workspaces/{WORKSPACE_ID}/time-entries/{timer_id}"
    data = {
        "projectId": WORK_PROJECT_ID,
        "start": start_time,
        "end": end_time,
    }
    response = put_clockify_request(endpoint, api_key, data)


# Get all time entries, filter out the ones without an end date, that is the currently running one
# Returns only id and start
def get_latest_time_entry(api_key):
    endpoint = f"workspaces/{WORKSPACE_ID}/user/{USER_ID}/time-entries"
    response = get_clockify_request(endpoint, api_key)
    latest_entry = list(
        filter(lambda entry: entry["timeInterval"]["end"] == None, response.json())
    )
    if len(latest_entry) > 0:
        latest_entry = latest_entry[0]
        return {
            "id": latest_entry["id"],
            "start": latest_entry["timeInterval"]["start"],
        }
    return None


# Collects statistics over all days worked since OLDEST_DATE
def print_average_stats(api_key):
    endpoint = f"workspaces/{WORKSPACE_ID}/user/{USER_ID}/time-entries"
    response = get_clockify_request(endpoint, api_key)

    workhours_per_day = {}
    for r in response.json():
        time_interval = r["timeInterval"]
        date_key = get_key_from_datestring(time_interval["start"])
        if parse(date_key) < parse(OLDEST_DATE):
            continue
        # Handle situation when timer is currently running
        if time_interval["end"] is None:
            time_interval["end"] = get_current_time_iso_zero_offset()
        workseconds = parse(time_interval["end"]) - parse(time_interval["start"])
        workhours = workseconds.seconds / 3600
        workhours_per_day[date_key] = ( workhours_per_day.get(date_key, 0) + workhours)

    total_workhours = 0
    num_workdays = 0
    for day, workhours in workhours_per_day.items():
        weekday = parse(day).weekday()
        total_workhours += workhours
        if parse(day).weekday() < 5:
            num_workdays += 1

    total_workhours += ADDITIONAL_OVERTIME

    average_workhours = total_workhours / num_workdays
    percentage_expected = average_workhours / EXPECTED_WORKHOURS * 100
    hours_diff = (average_workhours - EXPECTED_WORKHOURS) * len(
        workhours_per_day.values()
    )

    print(f"Average workhours: {average_workhours:.2f}h")
    print(f"Percentage of expected: {percentage_expected:.0f}%")
    print(f"Time diff: {hours_diff:.2f}h")


def get_key_from_datestring(date_string):
    return parse(date_string).strftime("%Y-%m-%d")


def post_clockify_request(endpoint, api_key, json):
    endpoint = "/api/v1/" + endpoint
    url = urljoin(BASE_URL, endpoint)
    headers = {"X-Api-Key": api_key, "Content-Type": "application/json"}
    return requests.post(url, headers=headers, json=json)


def put_clockify_request(endpoint, api_key, json):
    endpoint = "/api/v1/" + endpoint
    url = urljoin(BASE_URL, endpoint)
    headers = {"X-Api-Key": api_key, "Content-Type": "application/json"}
    return requests.put(url, headers=headers, json=json)


def get_clockify_request(endpoint, api_key):
    endpoint = "/api/v1/" + endpoint
    url = urljoin(BASE_URL, endpoint)
    headers = {"X-Api-Key": api_key}
    return requests.get(url, headers=headers)


# This returns the current time in clockfys time zone
def get_current_time_iso_zero_offset():
    return datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")


def parse_args():
    if len(sys.argv) != 2:
        print_usage()
        exit(1)
    return sys.argv[1]


def pretty_time_delta(timedelta):
    seconds = timedelta.total_seconds()
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    if days > 0:
        return "%dd%dh%dm%ds" % (days, hours, minutes, seconds)
    elif hours > 0:
        return "%dh%dm%ds" % (hours, minutes, seconds)
    elif minutes > 0:
        return "%dm%ds" % (minutes, seconds)
    else:
        return "%ds" % (seconds,)


if __name__ == "__main__":
    main()
