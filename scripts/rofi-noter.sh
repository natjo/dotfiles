#!/bin/bash

set -e

# Inspired by https://github.com/rahriver/rofi-noter
EDITOR=vim

# If set to true, the "more notes" category is sorted by date, otherwise it's sorted by the alphabet
SORT_BY_DATE="true"

# Note file names
# The script will create them automatically if they don't exist yet
MAIN_NOTES_DIRECTORY="$HOME/Documents/notes"
MORE_NOTES_DIRECTORY="$MAIN_NOTES_DIRECTORY/more"
WORK_NOTE="$MAIN_NOTES_DIRECTORY/work.md"
PRIVATE_NOTE="$MAIN_NOTES_DIRECTORY/private.md"
TEMPORARY_NOTE="$MAIN_NOTES_DIRECTORY/temporary.md"
TODO_NOTE="$MAIN_NOTES_DIRECTORY/todo.md"
TIME_TRACKING_NOTE="$MAIN_NOTES_DIRECTORY/time-tracking.md"



main(){
    mkdir -p $MAIN_NOTES_DIRECTORY
    # Prompt the user to select which note to edit using Rofi
    chosen_note=$(echo -e "📋 Work Note\n📜 Private Note\n✔️ Todo Note\n🕰️ Time Tracking\n🔖 Temporary Note\n✏️ Create Note\n📚 More Notes" | rofi -dmenu -i -p "Choose a note:")
    # Set the directory based on the user's selection
    # if selected_dir is not empty
    if [[ -n $chosen_note ]]; then
        case $chosen_note in
            "📋 Work Note")
                open_note $WORK_NOTE
                ;;
            "📜 Private Note")
                open_note $PRIVATE_NOTE
                ;;
            "✔️ Todo Note")
                open_note $TODO_NOTE
                ;;
            "🕰️ Time Tracking")
                open_note $TIME_TRACKING_NOTE
                ;;
            "🔖 Temporary Note")
                open_note $TEMPORARY_NOTE
                ;;
            "✏️ Create Note")
                create_new_note
                ;;
            "📚 More Notes")
                list_more_notes
                ;;
        esac
    fi
}

open_note(){
    local document_to_open=$1
    i3-sensible-terminal -e "$EDITOR" "$document_to_open"
}

create_new_note(){
    chosen_note=$(rofi -dmenu -i -p "Name new note:")
    open_note "$MORE_NOTES_DIRECTORY/$chosen_note.md"
}

list_more_notes(){
    # Less relevant notes are here, it's also the place for newly created notes
    mkdir -p $MORE_NOTES_DIRECTORY

    get_more_notes_names
    dirs_to_select="$ret"

    chosen_note=$(echo -e "$dirs_to_select" | rofi -dmenu -i -p "Choose a note:")
    # Remove the emoji in the beginning of the note
    chosen_note=$(echo $chosen_note | cut -d ' ' -f2-)
    if [[ -n $chosen_note ]]; then
        open_note "$MORE_NOTES_DIRECTORY/$chosen_note.md"
    fi
}

get_more_notes_names(){
    dirs_to_select=""

    OIFS="$IFS"
    IFS=$'\n'
    if [[ $SORT_BY_DATE == "true" ]]; then
        # Get all markdown files in the more note directory, print the timestamp and use it to sort the output
        for abs_filename_with_timestamp in $(find /home/jonas/Documents/notes/more -type f -printf '%T@ %p\n' -name '*.md' | sort -nr); do
            abs_filename=$(echo $abs_filename_with_timestamp | cut -d' ' -f2-)
            filename=$(echo $(basename "$abs_filename") | cut -d'.' -f1)
            dirs_to_select="$dirs_to_select📑 $filename\n"
        done
        IFS="$OIFS"
    else
        for abs_filename in $MORE_NOTES_DIRECTORY/*; do
            echo $abs_filename
            filename=$(echo $(basename "$abs_filename") | cut -d'.' -f1)
            dirs_to_select="$dirs_to_select📑 $filename\n"
        done
    fi
    # Remove last new line, it would cause an empty option
    dirs_to_select=${dirs_to_select::-2}

    ret=$dirs_to_select
}

main
