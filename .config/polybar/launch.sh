#!/bin/bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# polybar -c ~/.config/polybar/config.ini main >> $HOME/logs/polybar-main.log 2>&1 &
polybar -c ~/.config/polybar/config.ini main &
