# Create backup user
sudo useradd -m -G sudo -d /home/backup_user backup_user
sudo passwd backup_user

# General upgrade
sudo apt update
sudo apt upgrade -y

# Basic required installs
sudo apt install -y git curl fonts-powerline unclutter redshift pavucontrol pulseaudio pulseeffects lsp-plugins nodejs compton feh htop bat tree vim-gtk

# Get oh-my-zsh
sudo apt install -y zsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
# Add zsh plugin I use
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

# Init dotfiles
git clone --bare git@gitlab.com:natjo/dotfiles.git $HOME/.dotfiles
git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME checkout
git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME config --local status.showUntrackedFiles no

# Setup vim
mkdir -p $HOME/.vim/{swapfiles,undo,view,backup}
## Install plug to manage plugins
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
## Start vim and run `:PlugInstall`

# Install i3
sudo apt install -y i3 rofi
## Install alternating layout script
sudo apt-get install python3-pip
pip3 install i3ipc
git clone https://github.com/olemartinorg/i3-alternating-layout $HOME/scripts/i3-alternating-layout
# Install greenclip (google it)
sudo apt install -y polybar
# Install playerctl, required by a polybar module (mpris, for Spotify)
sudo apt install -y playerctl
# Install Spotify (google it)


# GTK Appearance
sudo apt install lxappearance
# Start program, pick theme you like

# Misc
sudo apt install -y fonts-firacode
# Configure terminal to use firacode
mkdir $HOME/ptmp
