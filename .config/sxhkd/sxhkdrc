#
# wm independent hotkeys
#

# terminal emulator
alt + Return
    terminator

# Start vs code with a shortkey
alt + c
    code

# program launcher
alt + d
    zsh -c "rofi -show run -run-command"

alt + w
    zsh -c "rofi -show window"

# make sxhkd reload its configuration files:
alt + Escape
	pkill -USR1 -x sxhkd

#
# bspwm hotkeys
#

# quit/restart bspwm
alt + super + shift + {q,r}
	bspc {quit,wm -r}

# close
alt + shift + q
	bspc node -c

alt + super + shift + q
	bspc node -k

# alternate between the tiled and monocle layout
super + m
	bspc desktop -l next

# send the newest marked node to the newest preselected node
super + y
	bspc node newest.marked.local -n newest.!automatic.local

# swap the current node and the biggest window
alt + g
	bspc node -s biggest.window

#
# state/flags
#

# set the window state
alt + {t,shift + t,s}
	bspc node -t {tiled,pseudo_tiled,floating}

# toggle fullscreen
alt + f
    bspc node -t \~fullscreen

# set the node flags
super + ctrl + {m,x,y,z}
	bspc node -g {marked,locked,sticky,private}

#
# focus/swap
#

# focus or move the node in the given direction
alt + {_,shift + }{h,j,k,l}
	bspc node -{f,s} {west,south,north,east}

# focus the node for the given path jump
super + {p,b,comma,period}
	bspc node -f @{parent,brother,first,second}

# focus the next/previous window in the current desktop
alt + {_,shift + }c
	bspc node -f {next,prev}.local.!hidden.window

# focus the next/previous desktop in the current monitor
super + bracket{left,right}
	bspc desktop -f {prev,next}.local

# focus the last node/desktop
alt + {space,Tab}
	bspc {node,desktop} -f last

# focus the older or newer node in the focus history
alt + {o,i}
	bspc wm -h off; \
	bspc node {older,newer} -f; \
	bspc wm -h on

# focus or send to the given desktop
alt + shift + {1-9,0}
	bspc node -d '^{1-9,10}'

# Allow back and forth jumping
alt + {1-9,0}
    desktop='^{1-9,10}'; \
    bspc query -D -d "$desktop.focused" && bspc desktop -f last || bspc desktop -f "$desktop"

#
# preselect
#

# preselect the direction
alt + ctrl + {h,j,k,l}
	bspc node -p {west,south,north,east}

# preselect the ratio
alt + ctrl + {1-9}
	bspc node -o 0.{1-9}

# cancel the preselection for the focused node
alt + ctrl + shift + space
	bspc node -p cancel

# cancel the preselection for the focused desktop
alt + ctrl + space
	bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

#
# move/resize
#

# Expand/contract a window by moving one of its side outward/inward
alt + r : {h,j,k,l}
    STEP=20; SELECTION={1,2,3,4}; \
    bspc node -z $(echo "left -$STEP 0,bottom 0 $STEP,top 0 -$STEP,right $STEP 0" | cut -d',' -f$SELECTION) || \
    bspc node -z $(echo "right -$STEP 0,top 0 $STEP,bottom 0 -$STEP,left $STEP 0" | cut -d',' -f$SELECTION)

# move a floating window
alt + {Left,Down,Up,Right}
	bspc node -v {-20 0,0 20,0 -20,20 0}

# Rotate window siblings
alt + R
    bspc node @parent -R 90

# balance all windows to use same area
alt + b
    bspc node @/ -B

# Volume
super + e
    bash $HOME/scripts/mediactrl.sh volume-inc
super + q
    bash $HOME/scripts/mediactrl.sh volume-dec
super + w
    bash $HOME/scripts/mediactrl.sh volume-toggle

super + a
    bash $HOME/scripts/mediactrl.sh spotify-previous
super + d
    bash $HOME/scripts/mediactrl.sh spotify-next
super + s
    bash $HOME/scripts/mediactrl.sh spotify-toggle-playPause

# Lock Screen
alt + shift + x
    multilockscreen --lock

# Lock Screen and suspend
alt + ctrl + shift + s
multilockscreen --lock & systemctl suspend
