#!/bin/sh
xrandr --output DVI-I-5-4 --off --output DVI-I-4-3 --off --output DVI-I-3-2 --off --output eDP-1-1 --mode 1366x768 --pos 0x0 --rotate normal --output DP-1-1 --primary --mode 2560x1440 --pos 1366x0 --rotate normal --output HDMI-1-1 --off --output DP-1-2 --off --output HDMI-1-2 --off --output DP-1-3 --off --output DVI-I-2-1 --off
